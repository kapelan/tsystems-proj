﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;


namespace wordcounter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void pathOpenButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog path = new OpenFileDialog();
            if (path.ShowDialog() == DialogResult.OK)
            {
                pathBox.Text = path.InitialDirectory + path.FileName;
                textBoxOriginal.Text = File.ReadAllText(path.FileName);
            }
        }

        private void goButton_Click(object sender, EventArgs e)
        {
            String ttBuffer = textBoxOriginal.Text;

            Dictionary<String, Int64> result = new Dictionary<String, Int64>();

            ttBuffer = Regex.Replace(ttBuffer, "[.?!)(,:«»\n\r]", " ");

            String[] ttArray = ttBuffer.ToLower().Split(new char[] { ' ' });

            String mStrTmp = "";

            for (int i=0; i<ttArray.Length; i++)
            {
                if (!result.ContainsKey(ttArray[i]))
                {
                    result.Add(ttArray[i], 1);
                }
                else
                {
                    result[ttArray[i]]++;
                }
            }

            // something to remove
            result.Remove("-");
            result.Remove("");
            // end

            foreach (var pairs in result.OrderByDescending(key => key.Value)) {
                mStrTmp+= pairs.Key + " - " + pairs.Value + "\r\n";                                   
            }

            textBoxResult.Text = mStrTmp;
        }
    }
}